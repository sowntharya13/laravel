composer update
chmod -R g+w public/uploads/
#chmod -R 0777 storage/
php artisan cache:clear
php artisan config:clear
php artisan view:clear
php artisan migrate
php artisan db:seed ModulesTablesDataSeeder
php artisan db:seed RouteslistTableDataSeeder
php artisan apitoken:daily
